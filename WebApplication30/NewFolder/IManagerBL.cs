﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication30.Core.Abstractions
{
    public interface IManagerBL
    {
        IEnumerable<Manager> GetManagers();
        Manager AddManagers(Manager manager);
        bool RemoveManagers(int id);
        bool UpdateManagers(Manager manager, int id); 
    }
}
