﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication30;
using WebApplication30.Core.Abstractions;
using WebApplication30.DAL;
using WebApplication30.BLL.Operations;
using WebApplication30.Controllers; 
using System.Linq;

namespace WebApplication30.BLL.Operations
{
    public class ManagerBL : IManagerBL
    {
        public ManagerController(ManagerDbContext managerDbContext)
        {
            this.managerDbContext = managerDbContext; 
        }
        private static readonly string[] Manager_Name = new[]
        {
            "Ann" , "Alicia" , "Ariana" , "James" , "John" , "Courtney" , "Cloe" , "Billie"
        };

        private static readonly string[] Manager_Surname = new[]
        {
            "Cordan" , "Anderson" , "Grande" , "Smith" , "Legend" , "Kardashian" , "Kardashian" , "Eilish"
        };

        private static readonly string[] Manager_Country = new[]
        {
            "USA" , "Italy" , "Montenegro" , "Albania" , "Gerany" , "Finland" , "Russia" , "Ukraine"
        };

        private static readonly string[] Manager_City = new[]
        {
            "Washingthon" , "Rome" , "Podgorica" , "Bucharest" , "Hambourg" , "Moscow" , "Kyiv"
        }; 
        private ManagerDbContext managerDbContext; 


        public  Manager AddManager(Manager manager)
        {
            managerDbContext.Managers.Add(manager);
            managerDbContext.SaveChanges();

            return manager; 
        }

        public bool RemoveManager(int id)
        {
            var rmv = managerDbContext.Managers.Find(id);
            if (rmv == null)
            {
                return false;
            }

            managerDbContext.Managers.Remove(rmv);
            managerDbContext.SaveChanges();

            return true; 
        }

        public bool UpdateManager(Manager manager, int id)
        {
            var managerToUpdate = managerDbContext.Managers.Find(id);
            if(managerToUpdate == null)
            {
                return false;
            }


            managerToUpdate.Id = manager.Id;
            managerToUpdate.Age = manager.Age; 
            managerToUpdate.FirstName = manager.FirstName;
            managerToUpdate.LastName = manager.LastName;
            managerToUpdate.Country = manager.Country;
            managerToUpdate.City = manager.City;

            managerDbContext.Managers.Update(manager);
            managerDbContext.SaveChanges();


            return true; 
        }

        public IEnumerable<Manager> GetManagers()
        {
            var rng = new Random();
            return Enumerable.Range(1, 8).Select(index => new Manager
            {
                Id = rng.Next(1, 8),
                Age = rng.Next(0, 70),
                Country = Manager_Country[rng.Next(Manager_Country.Length)],
                City = Manager_City[rng.Next(Manager_City.Length)],
                FirstName = Manager_Name[rng.Next(Manager_Name.Length)],
                LastName = Manager_Surname[rng.Next(Manager_Surname.Length)]
            });
        } 
    }
}
