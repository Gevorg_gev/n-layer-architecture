using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebApplication30
{
    [ApiController]
    [Route("[controller]")] 
    public class ManagerController : ControllerBase 
    {

        public ManagerController(ManagerDbContext managerDbContext)
        {
            this.managerDbContext = managerDbContext; 
        }
        

        private ManagerDbContext managerDbContext; 

        [HttpGet]
        public IEnumerable GetManagers()
        {
            var rng = new Random();
            
        }

        [HttpPost]
        public IActionResult AddManagers([FromBody] Manager manager)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            managerDbContext.Managers.Add(manager);
            managerDbContext.SaveChanges();


            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult RemoveManagers([FromRoute] int id)
        {
            var rmv = managerDbContext.Managers.Find(id);
            if(rmv == null)
            {
                return BadRequest("Wrong Id\n Please try again...");
            }

            managerDbContext.Managers.Remove(rmv);
            managerDbContext.SaveChanges();


            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult UpdateManagers([FromBody] Manager manager, [FromRoute] int id)
        {
            var managerToUpdate = managerDbContext.Managers.Find(id);
            if(managerToUpdate == null)
            {
                return BadRequest("Wrong Id\n Please try again...");
            }


            managerToUpdate.Id = manager.Id;
            managerToUpdate.Age = manager.Age;
            managerToUpdate.FirstName = manager.FirstName;
            managerToUpdate.LastName = manager.LastName;
            managerToUpdate.Country = manager.Country;
            managerToUpdate.City = manager.City; 

            managerDbContext.Managers.Update(manager);
            managerDbContext.SaveChanges();


            return Ok(); 
        }
    }
}